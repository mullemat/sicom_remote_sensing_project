from src.methods.cherki_inssaf.interpolation import interpolation
from src.methods.cherki_inssaf.pansharpening import pansharpening
from src.methods.cherki_inssaf.fusion import fusion
import numpy as np
from scipy.ndimage import gaussian_filter





def run_reconstruction(pan, ms1, ms2):
 
    ms1_interpolated = interpolation(ms1, pan.shape)
    ms2_interpolated = interpolation(ms2, pan.shape)

    ms1_pansharpened = pansharpening(ms1_interpolated,pan)
    ms2_pansharpened = pansharpening(ms2_interpolated,pan)
   
    reconstructed_image = fusion(pan, ms1_pansharpened, ms2_pansharpened)
 

    result = np.copy(reconstructed_image)
    for i in range(reconstructed_image.shape[0]):
        for j in range(reconstructed_image.shape[1]):
            result[i,j,:] = gaussian_filter(result[i,j,:],sigma=1) 


    return result

    