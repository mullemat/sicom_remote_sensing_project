from skimage.filters import gaussian
import numpy as np

def fusion(pan_image, ms1_image, ms2_image):
    sig_noise = 0.5
    ms2_image = gaussian(ms2_image,sigma=sig_noise, channel_axis=2)
    ms1_image = gaussian(ms1_image,sigma=sig_noise, channel_axis=2)
    pan_image = gaussian(pan_image,sigma=sig_noise, channel_axis=2)
    # Assigning more weight to the ms2 image since it has more bands
    weight_ms1 = 0.1  #(0.3)
    weight_ms2 = 0.9   #(0.7)

    #extending ms1_image to have the same number of bands as ms2_image
    ms1_extended = np.repeat(ms1_image, ms2_image.shape[2] // ms1_image.shape[2], axis=2)

    reconstructed_image = weight_ms1 * ms1_extended + weight_ms2 * ms2_image

    return reconstructed_image