"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
from scipy.ndimage import zoom
import pandas as pd

def upsample_multispectral_channels(P, M):
    """
    Upsample the channels of multispectral images to match the resolution of the panchromatic image.

    Parameters:
    - P: Panchromatic image (numpy array)
    - M: Multispectral image (numpy array)

    Returns:
    - upsampled_M: Upsampled multispectral image (numpy array)
    """

    upsampled_channels = []

    # Iterate over each channel in M
    for channel in range(M.shape[2]):
        channel_data = M[:, :, channel]
        zoom_factor = [float(P.shape[0]) / M.shape[0], float(P.shape[1]) / M.shape[1]]
        upsampled_channel = zoom(channel_data, zoom_factor, order=2)
        upsampled_channels.append(upsampled_channel)
    upsampled_M = np.stack(upsampled_channels, axis=-1)

    return upsampled_M

def Generalised_CS(M,P):
    """Component substitution for the generalised CS method.


    Args:
        M (np.ndarray): MS1 or MS2 image.
        P (np.ndarray): Panchromatic image.

    Returns:
        estimated_image: Reconstruction of the MS image.
    """
    estimated_image=np.zeros_like(M)
    N=M.shape[2]
    for idx in range(N):
        estimated_image[:,:,idx]=M[:,:,idx]+(P-(1/N)*np.sum(M,axis=2))
    return estimated_image


def find_closest_band(csv_file_path='data/WV3_VNIRSWIR_Spectral_Responses.csv', bandwidths= list(pd.read_csv('data/Image_wavelengths.csv')['wavelengths'])):
    """
    Find the closest band for each bandwidth in a list and then returns the index of max bands.

    Parameters:
    - csv_file_path: Path to the CSV file with spectral responses.
    - bandwidths: List of bandwidths for which to find the closest bands.

    Returns:
    - bands: A list of the highest index of the band in MS2 corresponding to the i-th band of MS1.
    """
    df = pd.read_csv(csv_file_path,index_col='wavelengths')
    result_dict = {}

    for i,bandwidth in enumerate(bandwidths):
        # Find the index of the closest wavelength
        closest_band_index = np.argmin(np.abs(df.iloc[:, 1:].idxmax(axis=0).astype(float) - bandwidth))
        result_dict[i] = closest_band_index + 1
    bands=[0]
    bands.extend([max([key for key in result_dict if result_dict[key] == value]) for value in range(1, 9)])
    return bands



def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    # Upsample the channels of the multispectral images
    upsampled_M1 = upsample_multispectral_channels(pan, ms1)
    upsampled_M2 = upsample_multispectral_channels(pan, ms2)

    # Perform the component substitution to MS1 for the intermediate spectral resolution
    res_intermediate=Generalised_CS(upsampled_M1,pan)
    
    # Matching the bands of MS1 and MS2
    bands=find_closest_band()
    
    # Perform the component substitution to MS2 for the final spectral resolution
    print(bands)
    result=np.zeros_like(upsampled_M2)
    for i in range(len(bands)-1):
        result[:,:,bands[i]:bands[i+1]]=Generalised_CS(upsampled_M2[:,:,bands[i]:bands[i+1]],res_intermediate[:,:,i])
    
    return np.clip(result,0,1)


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####s    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
