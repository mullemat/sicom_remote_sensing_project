import numpy as np
from sklearn.decomposition import PCA
from scipy.ndimage import gaussian_filter1d

def standardize_data(array, mean_val, std_val):
    """Standardize an array to a specific mean and standard deviation."""
    standardized = std_val * (array - array.mean(axis=(0,1))) / array.std(axis=(0,1)) + mean_val
    return standardized

def adjust_image_contrast(input_image, reference_image):
    """Adjust contrast of an image based on mean and std of a reference image."""
    if input_image.ndim > 2:
        adjusted_image = input_image.copy()
        for channel in range(input_image.shape[2]):
            adjusted_image[:,:,channel] = adjust_image_contrast(input_image[:,:,channel], reference_image[:,:,channel])
        return adjusted_image
    else:
        ref_mean = reference_image.mean(axis=(0,1))
        ref_std = reference_image.std(axis=(0,1))
        return standardize_data(input_image, ref_mean, ref_std)

def process_images(panorama: np.ndarray, multispectral1: np.ndarray, multispectral2: np.ndarray) :
    """Process and filter panorama and multispectral images."""
    
    panorama_filtered = gaussian_filter1d(panorama, sigma=0.5)

    def pca_denoise(ms_image):
        rows, cols, channels = ms_image.shape
        ms_flat = np.zeros((channels, rows * cols))
        for i in range(channels):
            ms_flat[i, :] = ms_image[:, :, i].ravel()
        
        pca = PCA(n_components=4)
        pca_transformed = pca.fit_transform(ms_flat)
        ms_reconstructed = pca.inverse_transform(pca_transformed)
        ms_reconstructed = np.reshape(ms_reconstructed, (channels, rows, cols))
        return np.transpose(ms_reconstructed, [1, 2, 0])

    ms1_pca = pca_denoise(multispectral1)
    ms2_pca = pca_denoise(multispectral2)

    return panorama_filtered, ms1_pca, ms2_pca
