import numpy as np

def weighted_merge(ms1_sharpened: np.ndarray, ms2_resampled: np.ndarray, pan: np.ndarray) -> np.ndarray:
    """Merge ms1 and ms2 based on weighted average considering pan intensities.
    
    Args:
        ms1_sharpened: The first multispectral image.
        ms2_resampled: The second multispectral image.
        pan: The panchromatic image.
        
    Returns:
        np.ndarray: The merged multispectral image.
    """
    weights = pan / np.max(pan) # Compute the weights based on the pan intensities.
    weighted_ms1 = ms1_sharpened * weights[:, :, np.newaxis] # Apply the weights to the first image.
    weighted_ms2 = ms2_resampled * (1 - weights[:, :, np.newaxis]) # Apply the weights to the second image.
    merged = weighted_ms1 + weighted_ms2 # Merge the two images.
    return merged