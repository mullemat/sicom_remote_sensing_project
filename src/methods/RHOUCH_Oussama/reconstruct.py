"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
from src.methods.RHOUCH_Oussama.adjust_image import adjust_image
from src.methods.RHOUCH_Oussama.pansharpen import pansharpen
from src.methods.RHOUCH_Oussama.interpolate_channels import interpolate_channels
from src.methods.RHOUCH_Oussama.weighted_merge import weighted_merge
import numpy as np
import numpy as np

def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    ms1_adjusted = adjust_image(pan, ms1)
    ms2_adjusted = adjust_image(pan, ms2)
    
    sharpened = pansharpen(ms1_adjusted, pan)
    
    interpolated = interpolate_channels(sharpened, ms2_adjusted.shape[2])
    
    fused_image = weighted_merge(interpolated, ms2_adjusted, pan)
    
    
    return fused_image


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
