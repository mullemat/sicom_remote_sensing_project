import numpy as np
from skimage.filters import gaussian
import cv2

def adjust_image(pan: np.ndarray, ms: np.ndarray) -> np.ndarray:
    """Adjust the multispectral image to the panchromatic image.

    Args:
        pan: The panchromatic image.
        ms: The multispectral image.

    Returns:
        np.ndarray: The adjusted multispectral image.
    """
    ms_gauss = gaussian(ms, sigma=1, channel_axis=2) # Apply a gaussian filter to the multispectral image.

    ms_bicubic = cv2.resize(ms_gauss, (pan.shape[1], pan.shape[0]), interpolation=cv2.INTER_CUBIC) # Resize the multispectral image to the panchromatic image.

    return ms_bicubic