from skimage.filters import gaussian
from cv2 import resize
import cv2
import numpy as np
import scipy
from utils import psnr, ssim
from scipy.ndimage import gaussian_filter

def check_img_dimensions(x, y):
    """Check if two images have the same dimensions"""
    assert x.shape==y.shape, "Images have not the same dimension"

def norm_std(x, m, s):
    """Normalize an array in order to have a given mean and std"""
    
    x_norm = s * (x - x.mean(axis=(0,1))) / x.std(axis=(0,1)) + m

    return x_norm
    
def norm_std_img(x, y):
    """Normalize an image x in order to have a given mean and std from image y"""
    check_img_dimensions(x, y)

    if x.ndim > 2:
        x_norm = x
        for i in range(x.shape[2]):
            x_norm[:,:,i] = norm_std_img(x[:,:,i], y[:,:,i])
        return x_norm
    else:
        y_mean = y.mean(axis=(0,1))
        y_std = y.std(axis=(0,1))
    
        # print(y_mean, y_std)

        x_norm = norm_std(x, y_mean, y_std)
        return x_norm

def evaluate(res: np.ndarray, img: np.ndarray) -> None:
    """Evaluate how close is the image reconstructed image to the ground truth reference image"""
    print('PSNR: %.3f' %psnr(img, res))
    print('SSIM: %.3f' %ssim(img, res))

def bicubic_up_interpolation_cude_img(cube_img: np.ndarray, outputsize: tuple):
    heigth = int((outputsize[0]/cube_img.shape[0]) * cube_img.shape[0])
    width = int((outputsize[1]/cube_img.shape[1]) * cube_img.shape[1]) 
    dim = (width,heigth)
    res = cv2.resize(cube_img, dim , interpolation=cv2.INTER_CUBIC)
    return res


def extract_wv3_bands_from_ms2(ms2: np.ndarray) -> list:
    ms2_1 = ms2[:,:,0:14]     # Channel 1
    ms2_2 = ms2[:,:,14:28]    # Channel 2
    ms2_3 = ms2[:,:,28:40]    # Channel 3
    ms2_4 = ms2[:,:,40:46]    # Channel 4
    ms2_5 = ms2[:,:,46:53]    # Channel 5
    ms2_6 = ms2[:,:,53:58]    # Channel 6
    ms2_7 = ms2[:,:,58:68]    # Channel 7
    ms2_8 = ms2[:,:,68:81]    # Channel 8
    ms2_9 = ms2[:,:,81:99]    # Channel 9

    return [ms2_1, ms2_2, ms2_3, ms2_4, ms2_5, ms2_6, ms2_7, ms2_8, ms2_9]

def pansharpening_gaussian(pan_image: np.ndarray,ms_image: np.ndarray, w: np.ndarray, sigma: float) -> np.ndarray:
    n_bands = ms_image.shape[2]

    S = np.zeros(ms_image.shape)
    I = np.zeros(pan_image.shape)
    g = np.zeros(ms_image.shape)

    for i in range(n_bands):
        I[:,:]+= w[i] * ms_image[:,:,i]

    pan_image_normalized = norm_std_img(pan_image,I)
    conv_gaussian = gaussian(pan_image_normalized,sigma)

    for i in range(n_bands):
        g[:,:,i] = np.divide(ms_image[:,:,i] , I)
     
    for k in range(n_bands):
        S[:,:,k] = ms_image[:,:,k] + g[:,:,k]*(pan_image_normalized - conv_gaussian)

    return(S[:ms_image.shape[0],:ms_image.shape[1],:])

def filter_wv(ms: np.ndarray) -> np.ndarray:
    res = np.copy(ms)
    for i in range(ms.shape[0]):
        for j in range(ms.shape[1]):
            res[i,j,:] = gaussian_filter(res[i,j,:],sigma=1) 
    return res