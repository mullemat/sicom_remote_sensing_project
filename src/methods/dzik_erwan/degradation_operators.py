"""A file containing the degradations operators.
This file should NOT be modified.
"""


import numpy as np
from skimage.transform import rescale
from scipy.interpolate import interp1d

from utils import get_pan_response, get_ms1_response


def get_pan(img: np.ndarray, wv: np.ndarray, response_path: str, sigma: float = 0) -> np.ndarray:
    """Generates the panchromatic img from the ground truth img.

    Args:
        img (np.ndarray): Ground truth img.
        wv (np.ndarray): Wavelengths of the image to which the response will be applied.
        response_path (str): Path to the file containing the panchromatic response. Must end by '.csv'.
        sigma (float, optional): Standard deviation of the gaussian noise added to the acquisition. Defaults to 0.

    Returns:
        np.ndarray: Panchromatic img.
    """
    df = get_pan_response(response_path)
    interp = interp1d(df['wavelengths'], df['panchromatic'])
    filter = interp(wv)
    res = np.average(img, axis=-1, weights=filter)

    return np.clip(res + np.random.normal(0, sigma, res.shape), 0, 1)


def get_ms1(img: np.ndarray, wv: np.ndarray, response_path: str, sigma: float = 0) -> np.ndarray:
    """Generates the mid spatial resolution img from the ground truth img.

    Args:
        img (np.ndarray): Ground truth img.
        wv (np.ndarray): Wavelengths of the image to which the response will be applied.
        response_path (str): Path to the file containing the panchromatic response. Must end by '.csv'.
        sigma (float, optional): Standard deviation of the gaussian noise added to the acquisition. Defaults to 0.

    Returns:
        np.ndarray: ms1 img.
    """
    df = get_ms1_response(response_path)
    down = rescale(img, 0.5, anti_aliasing=True, channel_axis=2)
    res = np.zeros((down.shape[0], down.shape[1], df.shape[1] - 1))

    for k in range (1, df.shape[1]):
        interp = interp1d(df['wavelengths'], df[str(k)])
        filter = interp(wv)
        res[:, :, k - 1] = np.average(down, axis=-1, weights=filter)

    return np.clip(res + np.random.normal(0, sigma, res.shape), 0, 1)


def get_ms2(img: np.ndarray, sigma: float = 0) -> np.ndarray:
    """Generates the low spatial resolution img from the ground truth img.

    Args:
        img (np.ndarray): Ground truth img.
        sigma (float, optional): Standard deviation of the gaussian noise added to the acquisition. Defaults to 0.

    Returns:
        np.ndarray: ms2 img.
    """
    res = rescale(img, 0.25, anti_aliasing=True, channel_axis=2)

    return np.clip(res + np.random.normal(0, sigma, res.shape), 0, 1)


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
