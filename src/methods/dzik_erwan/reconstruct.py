"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""

import functions as fun
import numpy as np
from skimage.filters import gaussian



def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    # Performing the reconstruction.
    
    sigma1, sigma2 = 1.5, 1.1 # Optimal parameters for the pansharpening
    # Denoising the images
    sig_noise = 0.5
    ms2_ = gaussian(ms2,sigma=sig_noise, channel_axis=2)
    ms1_ = gaussian(ms1,sigma=sig_noise, channel_axis=2)
    pan_ = gaussian(pan,sigma=sig_noise, channel_axis=2)

    #interpolation
    ms2_rescaled = fun.bicubic_up_interpolation_cude_img(ms2_, (ms1.shape[0],ms1.shape[1]))

    #Selecting the wv3 band in ms2
    ms2_wv3_bands = fun.extract_wv3_bands_from_ms2(ms2_rescaled)
    n_wv3_bands = len(ms2_wv3_bands)    # The 9 channels of the wv3 but for ms2


    ms = np.zeros((ms1.shape[0], ms1.shape[1],ms2.shape[2]))
    
    #Perform the 9 pansharpening between ms1 and selected channels of ms2
    temp = [] 
    for wv3_ch in range(n_wv3_bands):
        W1 = 1/ms2_wv3_bands[wv3_ch].shape[2] * np.ones(ms2_wv3_bands[wv3_ch].shape[2]) #Weights of the pansharpening
        temp.append(fun.pansharpening_gaussian(ms1_[:,:,wv3_ch], ms2_wv3_bands[wv3_ch], W1, sigma=sigma1))
    np.concatenate( tuple(temp), axis=2, out = ms)
    del temp

    #interpolation
    ms = fun.bicubic_up_interpolation_cude_img(ms, (pan.shape[0],pan.shape[1]))
    W2 = np.ones(ms2.shape[2]) / ms2.shape[2] #Weights of the pansharpening

    res = fun.pansharpening_gaussian(pan_, ms, W2, sigma=sigma2)

    # Remove residual spectral noise 
    return fun.filter_wv(res)


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
