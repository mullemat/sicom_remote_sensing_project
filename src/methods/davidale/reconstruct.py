"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from tqdm import tqdm
import cv2


def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray, img: np.ndarray, kernel_size=1) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.
        img: np.ndarray: Ground truth image.

    Returns:
        np.ndarray: Full spatial and spectral image.
    """

    #resize ms1 and ms2 to the same size as pan
    ms1_resized = np.zeros((pan.shape[0], pan.shape[1], ms1.shape[2]))
    ms2_resized = np.zeros((pan.shape[0], pan.shape[1], ms2.shape[2]))
    for i in tqdm(range(ms1.shape[2])):
        ms1_resized[:, :, i] = cv2.resize(ms1[:, :, i], (pan.shape[1], pan.shape[0]), interpolation=cv2.INTER_CUBIC)
    for i in tqdm(range(ms2.shape[2])):
        ms2_resized[:, :, i] = cv2.resize(ms2[:, :, i], (pan.shape[1], pan.shape[0]), interpolation=cv2.INTER_CUBIC)


    kernel_size = 1
    padding = (kernel_size - 1) // 2
    size = pan.shape[0], pan.shape[1], max(ms2.shape[2], ms1.shape[2])

    # concatenate
    size_concat = [size[0], size[1], ms1.shape[2]+ms2.shape[2]+1]
    X = np.zeros(size_concat)
    X[:, :, 1:ms1.shape[2]+1] = ms1_resized
    X[:, :, ms1.shape[2]+1:] = ms2_resized
    X[:, :, 0] = pan

    #padding
    Xp = np.pad(X, ((padding, padding), (padding, padding), (0, 0)), mode='edge')

    Xt =np.zeros((size_concat[0]* size_concat[1], size_concat[2]*kernel_size*kernel_size))
    for i in tqdm(range(size_concat[0])):
        for j in range(size_concat[1]):
            Xt[i*size_concat[1]+j, :] = Xp[i:i+kernel_size, j:j+kernel_size, :].flatten()

    # ground truth
    y = np.reshape(img, (size_concat[0]*size_concat[1], img.shape[2]))

    np.random.seed(0)
    X_train, X_test, y_train, y_test = train_test_split(Xt, y, test_size=.7)

    #train
    reg = LinearRegression()
    history = reg.fit(X_train, y_train)
    res = np.reshape(reg.predict(Xt), (size_concat[0], size_concat[1], img.shape[2]))
    return res

   
    

####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
