import numpy as np
import cv2
import matplotlib.pyplot as plt

def low_pass_filter(pan, filter_size):
   
    kernel = np.ones((filter_size, filter_size), np.float32) / (filter_size**2)
    pan_filtered = cv2.filter2D(pan, -1, kernel)

    return pan_filtered

def arsis_method(M, P, filter_size, g=1):
    P = cv2.resize(P, (M.shape[1], M.shape[0]))
    P_low_pass = low_pass_filter(P, filter_size)
    pansharpened_image = M.copy()

    for k in range(M.shape[2]):
        Mk = M[:, :, k]

        pansharpened_band = Mk + g * (P - P_low_pass)
        pansharpened_band = np.clip(pansharpened_band, 0, 1)
        pansharpened_image[:, :, k] = pansharpened_band

    return pansharpened_image