from src.methods.fatima_ezzahra_ouahmane.component_substitution import component_substitution
from src.methods.fatima_ezzahra_ouahmane.generalized_scheme import cs_generalized_scheme
from src.methods.fatima_ezzahra_ouahmane.injection_scheme import multiplicative_injection_scheme
from src.methods.fatima_ezzahra_ouahmane.arsis import arsis_method
import numpy as np



def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> list:
    reconstructed_ms1_cs = component_substitution(pan, ms1)
    reconstructed_ms2_cs = component_substitution(pan, ms2)

    reconstructed_ms1_generalized = cs_generalized_scheme(ms1, pan)
    reconstructed_ms2_generalized = cs_generalized_scheme(ms2, pan)
  
    reconstructed_ms1_multiplicative = multiplicative_injection_scheme(ms1 , pan)
    reconstructed_ms2_multiplicative =multiplicative_injection_scheme(ms2, pan)
 
    reconstructed_ms1_arsi = arsis_method(ms1, pan, filter_size=3 , g=1)
    reconstructed_ms2_arsi = arsis_method(ms2, pan, filter_size=3 , g=1)

    # Retourner une liste d'images
    return [reconstructed_ms1_cs,reconstructed_ms2_cs , reconstructed_ms1_generalized ,reconstructed_ms2_generalized ,reconstructed_ms1_multiplicative,reconstructed_ms2_multiplicative ,reconstructed_ms1_arsi,  reconstructed_ms2_arsi]
