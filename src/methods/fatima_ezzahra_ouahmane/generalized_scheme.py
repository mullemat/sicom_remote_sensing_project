
import numpy as np
import matplotlib.pyplot as plt
import cv2 

def norm_std_image(img):
    img_min, img_max = np.min(img), np.max(img)
    return (img - img_min) / (img_max - img_min)

def cs_generalized_scheme(M, P):
    N = M.shape[2]
    pansharpened_image = M.copy()
    
    P = cv2.resize(P, (M.shape[1], M.shape[0]))
    P_normalized = norm_std_image(P)

    for k in range(N):
        M_k = M[:, :, k]
        I = np.sum((1/N) * M, axis=2)
        g = 1  
        pansharpened_band = M_k + g * (P_normalized - I)
        pansharpened_band = np.clip(pansharpened_band, 0, 1)
        pansharpened_image[:, :, k] = pansharpened_band

    return pansharpened_image
