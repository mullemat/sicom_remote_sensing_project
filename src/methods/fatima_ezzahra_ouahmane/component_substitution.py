import numpy as np
from skimage.color import rgb2hsv, hsv2rgb
from skimage.transform import resize


def component_substitution(pan, ms):
 
    ms_hsv = rgb2hsv(ms[:, :, :3])  
    pan_resized = resize(pan, ms_hsv[:, :, 2].shape, anti_aliasing=True)
    ms_value_component = ms_hsv[:, :, 2]
    pan_pansharpened_image = np.copy(ms_hsv)
    pan_pansharpened_image[:, :, 2] =  pan_resized
    pan_pansharpened_image[:, :, 2] = np.clip(pan_pansharpened_image[:, :, 2], 0, 1)
    pansharpened_rgb = hsv2rgb(pan_pansharpened_image[:, :, :3])
    
    return pansharpened_rgb




