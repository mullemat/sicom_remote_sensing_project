'''
This file as to be launched from the main .ipynb file
'''

import matplotlib.pyplot as plt
import numpy as np
from src.utils import get_ms1_response, load_wavelengths

MINIMUM_ABSORPTION = 0.01

def plot_wavelengths(ms2_wv, plot_ms2_wv=True):
    '''
    Simple function to plot the wavelengths of ms1 (WorldView-3 MS image at medium spatial resolution with 9 spectral bands)
    along with the 99 wavelengths of the HYDICE sensor (coarse spatial resolution)

    :param ms2_wv: wavelengths of the HYDICE sensor
    '''
    ms1_wv = get_ms1_response("data/WV3_VNIRSWIR_Spectral_Responses.csv")
    ms1_wv = ms1_wv.set_index('wavelengths')

    color = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'deeppink', 'darkorange', 'gray']
    for i in range(len(ms1_wv.columns)):
        plt.plot(ms1_wv.index, ms1_wv[str(i+1)], color[i])

    if plot_ms2_wv:
        for i in range(len(ms2_wv)):
            index = np.argmax(ms1_wv.loc[round(ms2_wv[i])]) if max(ms1_wv.loc[round(ms2_wv[i])]) > MINIMUM_ABSORPTION else 9
            plt.plot([ms2_wv[i], ms2_wv[i]], [0, 0.2], color[index])
            # plt.text(ms2_wv[i], -0.001, str(i)+" "+str(wv[i])+"nm", fontsize=10)

        plt.title("WorldView-3 and HYDICE wavelengths")
    plt.xlabel("sorted wavelengths (nm)")

    return color

def get_wavelengths_repartition(selection_limit = MINIMUM_ABSORPTION):
    '''
    Function which returns the MRMS band number associated to every LRMS wavelength (or 9 if the wavelength does not
    suit a band (whether it suits a band or not is tested using the parameter selection_limit).

    :param selection_limit: Minimum value of absorption to decide whether an LRMS wavelength is part of an MRMS band
    :return: The index of the corresponding MRMS band for every LRMS wavelengths
    '''
    ms1_wv = get_ms1_response("data/WV3_VNIRSWIR_Spectral_Responses.csv")
    ms1_wv = ms1_wv.set_index('wavelengths')
    ms2_wv = load_wavelengths("data/Image_wavelengths.csv")

    # Split the ms2_wv into 9 different patches matching the ms1_wv (if not matching, add to the 10th category)
    index = []
    for wv in ms2_wv:
        index.append(np.argmax(ms1_wv.loc[int(round(wv))]) if max(ms1_wv.loc[int(round(wv))]) > selection_limit else 9)

    return index

def get_best_matching_wavelengths():
    '''
    Function which returns, for every MRMS band, the LRMS wavelength which maximizes the absorption value of the MRMS
    bands.
    :return: List of the 9 index which maximize the absorption value of each MRMS band
    '''
    ms1_wv = get_ms1_response("data/WV3_VNIRSWIR_Spectral_Responses.csv")
    ms1_wv = ms1_wv.set_index('wavelengths')
    ms2_wv = load_wavelengths("data/Image_wavelengths.csv")

    # Returning the best ms2_wv wavelength for each band of ms1_mv
    best_index = []
    for i in range(9):
        best_index.append(np.argmax(np.array([ms1_wv.loc[int(round(wv))][i] for wv in ms2_wv])))

    # Plot
    color = plot_wavelengths(ms2_wv, False)
    for i in range(len(best_index)):
        plt.plot([ms2_wv[best_index[i]], ms2_wv[best_index[i]]], [0, 1.1], color[i])
    plt.title("WorldView-3 wavelengths with best matching HYDICE wavelengths")
    plt.xlabel("wavelengths (nm)")

    return best_index
