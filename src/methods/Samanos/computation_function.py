import numpy as np
from skimage.transform import rescale
from skimage.color import rgb2hsv,hsv2rgb
import src.utils as utils
import matplotlib.pyplot as plt

def multiply_ms1(ms1:np.ndarray , filter:np.ndarray , i:int)->np.ndarray:
    """Multiply the 3 dimensional matrix of the ms1 sensor with the corresponding pondering factor of each wavelength
    Args:
        ms1 (np.ndarray): ms1 image.
        filter (np.ndarray): Ponder factor of each wavelength.
        i (int): the channel to which the multiplication is applied.

    Returns:
        np.ndarray: multiplication of the corrected channel.
    """
    S = np.zeros((ms1.shape[0],ms1.shape[1]))
    for j in range(filter.shape[1]):
        S = S + ms1[:,:,j] * filter[i,j]
    return S

def closest_ms1(ms1:np.ndarray , filter:np.ndarray , i:int)->np.ndarray:
    """Multiply the 3 dimensional matrix of the ms1 sensor with the corresponding pondering factor of each wavelength
    Args:
        ms1 (np.ndarray): ms1 image.
        filter (np.ndarray): Ponder factor of each wavelength.
        i (int): the channel to which the multiplication is applied.

    Returns:
        np.ndarray: multiplication of the corrected channel.
    """
    max_index = filter[i,:].argmax()
    S = ms1[:,:,max_index]
    return S

def get_ms1_wavelength(ms1: np.ndarray, wavelengths: np.ndarray) -> np.ndarray:
    res = np.zeros((wavelengths.shape[0],ms1.shape[1]-1))
    for i in range(wavelengths.shape[0]):
        el = wavelengths[i]
        el1 = int(np.floor(el))
        mid = 1- (el - el1)
        res [i,:] = ms1[ el1 - 300, 1: ] * mid + ms1[ el1 - 300 + 1, 1: ] * (1-mid)
    return res

def get_ms1_wavelength_but1(ms1: np.ndarray, wavelengths: np.ndarray,col: int) -> np.ndarray:
    res = np.zeros((wavelengths.shape[0],ms1.shape[1]-2))
    for i in range(wavelengths.shape[0]):
        el = wavelengths[i]
        el1 = int(np.floor(el))
        mid = 1- (el - el1)
        res [i,:col] = ms1[ el1 - 300, 1:col+1 ] * mid + ms1[ el1 - 300 + 1, 1:col+1 ] * (1-mid)
        res [i, col:] = ms1[ el1 - 300, col+2: ] * mid + ms1[ el1 - 300 + 1, col+2: ] * (1-mid)
    return res


def get_pan_wavelength(pan: np.ndarray, wavelengths: np.ndarray) -> np.ndarray:
    res = np.zeros(wavelengths.shape)
    for i in range(wavelengths.shape[0]):
        el = wavelengths[i]
        el1 = int(np.floor(el))
        mid = 1- (el - el1)
        res [i] = pan[ el1 - 300, 1 ] * mid + pan[ el1 - 300 + 1, 1 ] * (1-mid)
    return res

def upscale_ms1(img: np.ndarray, shape: tuple) -> np.ndarray:
    """Upscales the ms1 acquisition to the shape of the desired reconstruction.

    Args:
        img (np.ndarray): ms1 image.
        shape (tuple): Shape of the output.

    Returns:
        np.ndarray: Upscaled image.
    """
    return rescale(img, 2, anti_aliasing=True, channel_axis=2)[:shape[0], :shape[1]]

def choose_upscaling_channel(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray, ponder:tuple) -> np.ndarray:
    """Compute the average of the 3 arrays along axis2

    Args:
        pan (np.ndarray): the pan array with 99 channels
        ms1 (np.ndarray): the ms1 array with 99 channels upscaled
        ms2 (np.ndarray): the ms2 array with 99 channels upscaled
        ponder (tuple): 3 coefficients for the ponderation of each image

    return:
        np.ndarray: upscaled image of the linear combination of the 3 images
    """
    res = np.zeros((pan.shape[0],pan.shape[1],ms2.shape[2]))
    for i in range (ms2.shape[2]):
        res[:,:,i] =  (ponder[0]*pan[:,:,i]+ponder[1]*ms1[:,:,i]+ponder[2]*ms2[:,:,i])
    return res

def improve_img2hsv(img:np.ndarray,pan:np.ndarray) -> np.ndarray:
    res_rgb = img
    res_hsv = rgb2hsv(res_rgb)
    res_hsv[:,:,2] = pan
    res_rgb = hsv2rgb(res_hsv)
    return res_rgb

def augment_dim_ms1(img: np.ndarray) -> np.ndarray:
    """Augment the dimension of the ms1 acquisition with the desired resolution to the desired number of channels.

    Args:
        img (np.ndarray): ms1 image.

    Returns:
        np.ndarray: Upchanneled image.
    """
    # Get the table of the values of the ms1 sensor
    ms1_responses_path = 'data/WV3_VNIRSWIR_Spectral_Responses.csv'
    sensor = utils.get_pan_response(ms1_responses_path).to_numpy()
    # Get the wavelength of the different channels of the image
    wavelengths_path = 'data/Image_wavelengths.csv'
    wl = utils.load_wavelengths(wavelengths_path)
    # Compute the ponderation coefficient of each of the channels along the channels of the ms1 sensor
    filter = get_ms1_wavelength(sensor,wl)
    img1 = np.zeros((img.shape[0],img.shape[1],99))
    # Compute the 99 version of the ms1 image with the corresponding ponderation coefficients
    for i in range(99):
        img1[:,:,i] = closest_ms1(img,filter,i)

    return img1

def freq_factor(img:np.ndarray) -> np.ndarray:
    res = np.zeros(img.shape[2])
    res = np.sum(img,axis=(0,1))
    res = res / res.max()
    # plt.figure()
    # plt.plot(res)
    # plt.title("Curve of the spectral intensity capted by the sensor 2")
    # plt.xlabel("Spectral Channel")
    # plt.ylabel("Intensity (Normalized)")
    return res

def freq_factor_ms1(img:np.ndarray) -> np.ndarray:
    img1 = augment_dim_ms1(img)
    return freq_factor(img1)

def get_pan_ponder() -> np.ndarray:
    pan_responses_path = 'data/QB_Spectral_Response.csv'
    sensor = utils.get_pan_response(pan_responses_path).to_numpy()

    wavelengths_path = 'data/Image_wavelengths.csv'
    wl = utils.load_wavelengths(wavelengths_path)

    filter = get_pan_wavelength(sensor,wl)*1.5
    return filter

def get_ms1_ponder() -> np.ndarray:
    # Get the table of the values of the ms1 sensor
    ms1_responses_path = 'data/WV3_VNIRSWIR_Spectral_Responses.csv'
    sensor = utils.get_pan_response(ms1_responses_path).to_numpy()
    # Get the wavelength of the different channels of the image
    wavelengths_path = 'data/Image_wavelengths.csv'
    wl = utils.load_wavelengths(wavelengths_path)
    filter = get_ms1_wavelength(sensor,wl)
    filter = np.max(filter,axis=1)*1.75
    return filter

def get_ms2_ponder() -> np.ndarray:
    return np.ones((99))/2

