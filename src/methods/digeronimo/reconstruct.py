"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
from skimage import transform
from methods.digeronimo.somefunc import norm_std_img
from utils import save_image

def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """
    # Performing the reconstruction.

    ## PREPROCESSING
    # Rescale the images so that they have the same dimensions as the pan image
    n,m = pan.shape[0],pan.shape[1]

    ms1_ = transform.resize(ms1,(n,m)) #(307, 1280, 9)
    ms2_ = transform.resize(ms2,(n,m)) #(307, 1280, 99)
    
    # Enhance ms2 image thanks to ms1
    # Replace true color bands of ms2 (48,33,16) by true color bands of ms1 (4,2,0)

    ms = np.zeros_like(ms2_)

    for i in range(0,16):
        ms[:,:,i] = ms2_[:,:,i]

    ms[:,:,16] = ms1_[:,:,0]

    for i in range(17,33):
        ms[:,:,i] = ms2_[:,:,i]
        
    ms[:,:,33] = ms1_[:,:,2]

    for i in range(34,48):
        ms[:,:,i] = ms2_[:,:,i]

    ms[:,:,48] = ms1_[:,:,4]

    for i in range(49,99):
        ms[:,:,i] = ms2_[:,:,i]

    ## MULTIPLICATIVE INJECTION SCHEME
    # Perform the reconstruction thanks to the panchromatic image by the fusion scheme method
        
    w = 1/99 # weights
    I = np.zeros((ms.shape[0],ms.shape[1])) # intensity
    S = np.zeros((ms.shape[0],ms.shape[1],ms.shape[2])) # result

    for i in range(99):
        I[:,:] += ms[:,:,i]

    I = w*I

    for i in range(ms.shape[-1]):
        S[:,:,i] = ms[:,:,i] + np.multiply(np.divide(ms[:,:,i],I),(pan-I))

    hs_ms = norm_std_img(S,ms) # normalization step to keep the same dynamic as ms

    # Save image

    file_path = "output/reconstructed_img.npy"
    save_image(file_path, hs_ms)

    return hs_ms


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
