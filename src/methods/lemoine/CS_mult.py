from src.methods.lemoine.tool import norm_std_img
import numpy as np

## Component Substitution method for pansharpening

#  CS : Multiplicative injection

def CS_mult(pan,ms) :
    N = ms.shape[-1]                                # Number of spectral bands
    w = 1/N                                         # Weights (same)
    S = np.zeros(ms.shape)
    I = np.zeros(pan.shape)
    for i in range (N):
        I += ms[:,:,i] * w
    pan = norm_std_img(pan, I)                      # Normalisation
    for i in range (N):
        S[:,:,i] = np.multiply(ms[:,:,i],np.divide(pan,I))
    return S