import numpy as np
from sklearn.decomposition import PCA as pca
from src.methods.lemoine.tool import norm_std_img

## PCA method for pansharpening

def PCA(pan, ms):

    M, N = pan.shape
    m, n, C = ms.shape
    
    p = pca(n_components = C)                           # Perform PCA

    pca_ms = p.fit_transform(np.reshape(ms, (M*N, C)))  # Reshape
    pca_ms = np.reshape(pca_ms, (M, N, C))
    
    I = pca_ms[:, :, 0]

    pan = norm_std_img(pan, I)                          # Normalisation
    pca_ms[:, :, 0] = pan
    I_PCA = p.inverse_transform(pca_ms)                 # Inverse transform
    
    
    return I_PCA