from src.methods.lemoine.tool import norm_std_img
import numpy as np

## Component Substitution method for pansharpening

#  CS : Additive injection

def CS_add(pan,ms) :
    N = ms.shape[-1]                         # Number of spectral bands
    w = 1/N                                  # Weights (Same)
    g = 1                                    # Gains (None)
    S = np.zeros(ms.shape)
    I = np.zeros(pan.shape)
    for i in range (N-1):
        I += ms[:,:,i] * w 
    pan = norm_std_img(pan, I)               # Normalisation
    for i in range (N):
        S[:,:,i] = ms[:,:,i] + g*(pan-I)
    return S