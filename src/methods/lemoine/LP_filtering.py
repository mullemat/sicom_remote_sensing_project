from src.methods.lemoine.tool import norm_std_img
import numpy as np
from scipy import signal

## Component Substitution method for pansharpening

#  Low pass filtering

def LP_filtering (pan, ms):
    N = ms.shape[-1]
    g = 1                                    # Gains (None)

    S = np.zeros(np.shape(ms))
    H_lp = (1/9)*np.ones((3,3))              # Average filter

    pan_conv = signal.convolve2d(pan, H_lp, mode='same', boundary='fill')   # Perform filtering

    for i in range (N):                                                     # Additive scheme
        S[:,:,i] = ms[:,:,i]+g*(pan-pan_conv)

    S_norm = norm_std_img(S,ms)                                             # Normalization

    return (S_norm)