"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
from src.methods.lemoine.CS_add import CS_add
from src.methods.lemoine.Bicubic import bicubic_interpolation
from src.methods.lemoine.CS_mult import CS_mult
from src.methods.lemoine.LP_filtering import LP_filtering
from src.methods.lemoine.PCA import PCA
from src.methods.lemoine.Wavelet import Wavelet

def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """

    method = 'CS_add' # Choose the method

    # Perform bicubic interpolation on ms1
    bicubic = bicubic_interpolation(pan, ms1)


    if method == 'bicubic':
        return bicubic

    elif method == 'CS_add':
        # Component substitution additive scheme
        CS_A = CS_add(pan, bicubic)
        return CS_A

    elif method == 'CS_mult':
        # Component substitution multiplicative scheme
        CS_M = CS_mult(pan, bicubic)
        return CS_M

    elif method == 'LP':
        # Component substitution low pass filtering
        LP = LP_filtering(pan, bicubic)
        return LP
    
    elif method == 'PCA':
        # PCA
        pca = PCA(pan, bicubic)
        return pca
    
    elif method == 'Wavelet':
        # Wavelet
        wav = Wavelet(pan, bicubic)
        return wav


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
