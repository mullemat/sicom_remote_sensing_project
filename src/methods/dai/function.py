# Helper functions

import numpy as np
from sklearn import decomposition
from scipy.ndimage import gaussian_filter

def check_img_dimensions(x, y):
    """Check if two images have the same dimensions"""
    assert x.shape==y.shape, "Images have not the same dimension"

def norm_std(x, m, s):
    """Normalize an array in order to have a given mean and std"""
    
    x_norm = s * (x - x.mean(axis=(0,1))) / x.std(axis=(0,1)) + m

    return x_norm


def norm_std_img(x, y):
    """Normalize an image x in order to have a given mean and std from image y"""
    #check_img_dimensions(x, y)

    if x.ndim > 2:
        x_norm = x
        for i in range(x.shape[2]):
            x_norm[:,:,i] = norm_std_img(x[:,:,i], y[:,:,i])
        return x_norm
    else:
        y_mean = y.mean(axis=(0,1))
        y_std = y.std(axis=(0,1))
    
        # print(y_mean, y_std)

        x_norm = norm_std(x, y_mean, y_std)
        return x_norm
    
def filter(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray)-> np.ndarray:


    ms1_row,ms1_col,ms1_channel = ms1.shape
    ms2_row,ms2_col,ms2_channel = ms2.shape

    #pan denoising
    pan_f = gaussian_filter(pan,sigma=0.5)

    #ms1 denoising
    ms1_flatten = np.zeros((ms1_channel,ms1_row*ms1_col))
    for k in range(ms1_channel):
        ms1_flatten[k,:]=np.ravel(ms1[:,:,k])


    pca = decomposition.PCA(n_components=4)
    pca.fit(ms1_flatten)
    pca_ms1 = pca.transform(ms1_flatten)
    ms1_pca_result = pca.inverse_transform(pca_ms1)


    ms1_pca_result = np.reshape(ms1_pca_result,(ms1_channel,ms1_row,ms1_col))
    ms1_pca_result = np.transpose(ms1_pca_result, [1,2,0])


    #ms2 denoising
    ms2_flatten = np.zeros((ms2_channel,ms2_row*ms2_col))
    for k in range(ms2_channel):
        ms2_flatten[k,:] = np.ravel(ms2[:,:,k])

    pca = decomposition.PCA(n_components=4)
    pca.fit(ms2_flatten)
    pca_ms2 = pca.transform(ms2_flatten)
    ms2_pca_result = pca.inverse_transform(pca_ms2)


    ms2_pca_result = np.reshape(ms2_pca_result,(ms2_channel,ms2_row,ms2_col))
    ms2_pca_result = np.transpose(ms2_pca_result, [1,2,0])

    return pan_f,ms1_pca_result,ms2_pca_result
                 
    


